table_set_default forward _drop
table_indirect_create_group ecmp_group
table_indirect_create_group ecmp_group
table_indirect_create_member ecmp_group _drop
table_add forward set_dmac 10.1.0.57 => 00:0a:00:00:00:15
table_indirect_create_member ecmp_group set_nhop 10.1.0.57 1
table_indirect_add_member_to_group ecmp_group 1 0
table_add forward set_dmac 10.1.0.60 => 00:0a:00:00:00:18
table_indirect_create_member ecmp_group set_nhop 10.1.0.60 2
table_indirect_add_member_to_group ecmp_group 2 0
table_add forward set_dmac 10.1.0.63 => 00:0a:00:00:00:1b
table_indirect_create_member ecmp_group set_nhop 10.1.0.63 3
table_indirect_add_member_to_group ecmp_group 3 0
table_add forward set_dmac 10.1.0.78 => 00:0c:00:00:00:33
table_indirect_create_member ecmp_group set_nhop 10.1.0.78 254
table_indirect_add_member_to_group ecmp_group 4 1
table_indirect_set_default ecmp_group 0
table_indirect_add_with_group ecmp_group 10.0.1.4/32 => 0
table_indirect_add_with_group ecmp_group 10.0.2.4/32 => 0
table_indirect_add_with_group ecmp_group 10.0.3.4/32 => 0
table_indirect_add_with_group ecmp_group 10.0.4.4/32 => 0
table_indirect_add_with_group ecmp_group 10.0.5.4/32 => 0
table_indirect_add_with_group ecmp_group 10.0.6.4/32 => 0
table_indirect_add_with_group ecmp_group 10.0.7.4/32 => 1
table_indirect_add_with_group ecmp_group 10.0.8.4/32 => 1
table_indirect_add_with_group ecmp_group 10.0.9.4/32 => 1
