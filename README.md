# Expeditus on P4

This is the development guide for Expeditus with P4.If you are just using Expeditus in P$ switch, you only need the output json from P4 compiler.

This respository should be placed at 
```
#!python

/behavioral-model3/targets/expeditus
```


#P4 Compilation
You have to install P4 compiler before compiling P4 program. P4 compiler can be downloaded from (https://github.com/p4lang/p4c-bm). However, as we used the modified P4 switch target, we have added extra functions in switch API for P4 language. We have to replace the the p4c-bm/p4c_bm/primitive.json with our primitive.json from P4 compiler respository to add the new functions to the P4 compiler.

You can compile with following code after P4 compiler is installed and "primitive.json" is replaced


```
#!shell

sudo p4c-bmv2 solution_2/p4src/simple_router.p4 --json solution_2/simple_router.json
```



#Files
##veth_setup.sh

```
#!shell

sudo ./veth_setup.sh
```

Running this file can let the machine simulate to have multiple network interface.
Setup the virtual ethernets in linux. 
Add virtual ethernet as pairs.
Make them up.
Set options for the ethernet pairs.


##run_as_demo_network.sh

```
#!shell

run_as_demo_network.sh
```
This can run fat tree network on mininet. After running this to simulate the network. You need to run 
```
#!shell

./add_entries_from_py.sh
```
for running command on P4 switches. After that, all hosts are already connected.
This command also add create the command for the runtime CLI for the switches named 
```
#!python

909x.txt
```

For the real machine, some configurations are needed. This command also generate the commands as 
```
#!python

909x_config.txt
```




##run_as_xxxx_final.sh
By running this command can run the machine as the P4 switch with the id xxxx(eg. 9090 is a Core switch).

It is important to bind the switch port to the network interface in the file. For example:

```
#!shell

sudo $SWITCH_PATH solution_2/simple_router.json \ 
-i 1@eth6 -i 2@eth4 -i 254@eth3 -i 253@eth7&

```
the "-i" parameter is used for binding switch ports. My Expeditus P4 switch can use port 1-16 as upstream port. This example bind port 1 with eth6 and port 2 of switch as eth4.

Use "screen" command could make switch run in background and switch does not terminated after SSH connection closed.eg.

```
#!shell

screen ./run_as_909x_final.sh
```
