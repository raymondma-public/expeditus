#!/bin/bash

# Copyright 2013-present Barefoot Networks, Inc. 
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

source $THIS_DIR/env.sh

#P4C_BM_SCRIPT=$P4C_BM_PATH/p4c_bm/__main__.py

SWITCH_PATH="../simple_switch/simple_switch"

# CLI_PATH="../../tools/runtime_CLI.py"
CLI_PATH="../simple_switch/sswitch_CLI"

# Probably not very elegant but it works nice here: we enable interactive mode
# to be able to use fg. We start the switch in the background, sleep for 2
# minutes to give it time to start, then add the entries and put the switch
# process back in the foreground
set -m
#sudo p4c-bmv2 solution_2/p4src/simple_router.p4 --json solution_2/simple_router.json
if [ $? -ne 0 ]; then
echo "p4 compilation failed"
exit 1
fi
# This gets root permissions, and gives libtool the opportunity to "warm-up"
sudo $SWITCH_PATH >/dev/null 2>&1 --thrift-port 9094
sudo $SWITCH_PATH  solution_2/simple_router.json --thrift-port 9094\
    -i 254@eth6 -i 253@eth3&
#--log-console&



    
    

sleep 2
echo "**************************************"
echo "Sending commands to switch through CLI"
echo "**************************************"
# $CLI_PATH --json solution_2/simple_router.json < solution_2/commands.txt
$CLI_PATH  solution_2/simple_router.json < ./9094.txt 9094
echo "READY!!!"
fg
