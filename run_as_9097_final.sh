#!/bin/bash
THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}")" && pwd )
source $THIS_DIR/env.sh
SWITCH_PATH="../simple_switch/simple_switch"
CLI_PATH="../simple_switch/sswitch_CLI"
set -m
if [ $? -ne 0 ]; then
echo "p4 compilation failed"
exit 1
fi
sudo $SWITCH_PATH >/dev/null 2>&1
sudo $SWITCH_PATH solution_2/simple_router.json  --thrift-port 9097 \
-i 1@eth5 -i 2@eth3 -i 254@eth2 &

sleep 2
echo "*** Send command to switch ***"
$CLI_PATH solution_2/simple_router.json < ./9097.txt 9097
echo "READY!!!"
fg
