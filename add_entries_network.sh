#!/bin/sh

a=0
port=9089


while [ "$a" -lt 10 ]
do
	port=`expr $port + 1 `
	path="./solution_2/commands$port.txt"
	echo $path
	./runtime_CLI --thrift-port $port < $path
	a=`expr $a + 1`
done
