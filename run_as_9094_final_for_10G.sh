#!/bin/bash
THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}")" && pwd )
source $THIS_DIR/env.sh
SWITCH_PATH="../simple_switch/simple_switch"
CLI_PATH="../simple_switch/sswitch_CLI"
set -m
if [ $? -ne 0 ]; then
echo "p4 compilation failed"
exit 1
fi
sudo $SWITCH_PATH >/dev/null 2>&1
sudo $SWITCH_PATH solution_2/simple_router.json  --thrift-port 9094 \
 -i 254@eth7 -i 253@eth4 &

sleep 2
echo "*** Send command to switch ***"
$CLI_PATH solution_2/simple_router.json < ./9094.txt 9094
echo "READY!!!"
fg
