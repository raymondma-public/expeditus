table_set_default ToR_src_2nd_stage_table ToR_src_2nd_stage_action
table_set_default ToR_modify_cloned_packet modify_cloned_packet
table_set_default ingress_upstream_table get_upstream_num 2
table_set_default egress_upstream_table get_upstream_num 2
table_set_default forward _drop
table_set_default expeditus_first_forward _drop
table_set_default expeditus_stage1_tor_target_forward _drop
table_set_default set_CLI_thrift_port_table set_CLI_thrift_port 9099
table_set_default expeditus_stage1_tor_target_remove_header remove_expeditus_header
table_set_default expeditus_header_table add_expeditus_tags
table_set_default increment_table increment_action
table_set_default increment_SYN_table increment_SYN_action
table_set_default switch_role_table init_switch_role 0
table_set_default expeditus_stage1_tor_target_compute_egress_port compute_and_set_later_forward_egress 2
table_add increment_table increment_action
table_indirect_create_group ecmp_group
table_indirect_create_group expeditus_group
table_indirect_create_group expeditus_stage1_tor_target_group
table_indirect_create_group ecmp_group
table_indirect_create_group expeditus_group
table_indirect_create_group expeditus_stage1_tor_target_group
table_indirect_create_group ecmp_group
table_indirect_create_group expeditus_group
table_indirect_create_group expeditus_stage1_tor_target_group
table_indirect_create_member ecmp_group _drop
table_indirect_create_member expeditus_group _drop
table_indirect_create_member expeditus_stage1_tor_target_group _drop
table_add forward set_dmac 10.1.0.21 => 00:0b:00:00:00:0d
table_add expeditus_first_forward set_dmac 10.1.0.21 => 00:0b:00:00:00:0d
table_add expeditus_stage1_tor_target_forward set_dmac 10.1.0.21 => 00:0b:00:00:00:0d
table_indirect_create_member ecmp_group set_nhop 10.1.0.21 1
table_indirect_create_member expeditus_group set_nhop 10.1.0.21 1
table_indirect_create_member expeditus_stage1_tor_target_group set_nhop 10.1.0.21 1
table_indirect_add_member_to_group ecmp_group 1 0
table_indirect_add_member_to_group expeditus_group 1 0
table_indirect_add_member_to_group expeditus_stage1_tor_target_group 1 0
table_add forward set_dmac 10.1.0.24 => 00:0b:00:00:00:10
table_add expeditus_first_forward set_dmac 10.1.0.24 => 00:0b:00:00:00:10
table_add expeditus_stage1_tor_target_forward set_dmac 10.1.0.24 => 00:0b:00:00:00:10
table_indirect_create_member ecmp_group set_nhop 10.1.0.24 2
table_indirect_create_member expeditus_group set_nhop 10.1.0.24 2
table_indirect_create_member expeditus_stage1_tor_target_group set_nhop 10.1.0.24 2
table_indirect_add_member_to_group ecmp_group 2 0
table_indirect_add_member_to_group expeditus_group 2 0
table_indirect_add_member_to_group expeditus_stage1_tor_target_group 2 0
table_add forward set_dmac 10.0.3.4 => 00:04:00:00:03:00
table_add expeditus_first_forward set_dmac 10.0.3.4 => 00:04:00:00:03:00
table_add expeditus_stage1_tor_target_forward set_dmac 10.0.3.4 => 00:04:00:00:03:00
table_indirect_create_member ecmp_group set_nhop 10.0.3.4 254
table_indirect_create_member expeditus_group set_nhop 10.0.3.4 254
table_indirect_create_member expeditus_stage1_tor_target_group set_nhop 10.0.3.4 254
table_indirect_add_member_to_group ecmp_group 3 1
table_indirect_add_member_to_group expeditus_group 3 1
table_indirect_add_member_to_group expeditus_stage1_tor_target_group 3 1
table_add forward set_dmac 10.0.4.4 => 00:04:00:00:04:00
table_add expeditus_first_forward set_dmac 10.0.4.4 => 00:04:00:00:04:00
table_add expeditus_stage1_tor_target_forward set_dmac 10.0.4.4 => 00:04:00:00:04:00
table_indirect_create_member ecmp_group set_nhop 10.0.4.4 253
table_indirect_create_member expeditus_group set_nhop 10.0.4.4 253
table_indirect_create_member expeditus_stage1_tor_target_group set_nhop 10.0.4.4 253
table_indirect_add_member_to_group ecmp_group 4 2
table_indirect_add_member_to_group expeditus_group 4 2
table_indirect_add_member_to_group expeditus_stage1_tor_target_group 4 2
table_indirect_set_default ecmp_group 0
table_indirect_set_default expeditus_group 0
table_indirect_set_default expeditus_stage1_tor_target_group 0
table_indirect_add_with_group ecmp_group 10.0.1.4/32 => 0
table_indirect_add_with_group expeditus_group 10.0.1.4/32 => 0
table_indirect_add_with_group expeditus_stage1_tor_target_group 10.0.1.4/32 => 0
table_indirect_add_with_group ecmp_group 10.0.2.4/32 => 0
table_indirect_add_with_group expeditus_group 10.0.2.4/32 => 0
table_indirect_add_with_group expeditus_stage1_tor_target_group 10.0.2.4/32 => 0
table_indirect_add_with_group ecmp_group 10.0.3.4/32 => 1
table_indirect_add_with_group expeditus_group 10.0.3.4/32 => 1
table_indirect_add_with_group expeditus_stage1_tor_target_group 10.0.3.4/32 => 1
table_indirect_add_with_group ecmp_group 10.0.4.4/32 => 2
table_indirect_add_with_group expeditus_group 10.0.4.4/32 => 2
table_indirect_add_with_group expeditus_stage1_tor_target_group 10.0.4.4/32 => 2
mirroring_add 1 1
mirroring_add 2 2
mirroring_add 3 3
mirroring_add 4 4
mirroring_add 5 5
mirroring_add 6 6
mirroring_add 7 7
mirroring_add 8 8
mirroring_add 9 9
mirroring_add 10 10
mirroring_add 11 11
mirroring_add 12 12
mirroring_add 13 13
mirroring_add 14 14
mirroring_add 15 15
mirroring_add 16 16
mirroring_add 17 17
mirroring_add 18 18
mirroring_add 19 19
mirroring_add 20 20
mirroring_add 21 21
mirroring_add 22 22
mirroring_add 23 23
mirroring_add 24 24
mirroring_add 25 25
mirroring_add 26 26
mirroring_add 27 27
mirroring_add 28 28
mirroring_add 29 29
mirroring_add 30 30
mirroring_add 31 31
mirroring_add 32 32
mirroring_add 33 33
mirroring_add 34 34
mirroring_add 35 35
mirroring_add 36 36
mirroring_add 37 37
mirroring_add 38 38
mirroring_add 39 39
mirroring_add 40 40
mirroring_add 41 41
mirroring_add 42 42
mirroring_add 43 43
mirroring_add 44 44
mirroring_add 45 45
mirroring_add 46 46
mirroring_add 47 47
mirroring_add 48 48
mirroring_add 49 49
mirroring_add 50 50
mirroring_add 51 51
mirroring_add 52 52
mirroring_add 53 53
mirroring_add 54 54
mirroring_add 55 55
mirroring_add 56 56
mirroring_add 57 57
mirroring_add 58 58
mirroring_add 59 59
mirroring_add 60 60
mirroring_add 61 61
mirroring_add 62 62
mirroring_add 63 63
mirroring_add 64 64
mirroring_add 65 65
mirroring_add 66 66
mirroring_add 67 67
mirroring_add 68 68
mirroring_add 69 69
mirroring_add 70 70
mirroring_add 71 71
mirroring_add 72 72
mirroring_add 73 73
mirroring_add 74 74
mirroring_add 75 75
mirroring_add 76 76
mirroring_add 77 77
mirroring_add 78 78
mirroring_add 79 79
mirroring_add 80 80
mirroring_add 81 81
mirroring_add 82 82
mirroring_add 83 83
mirroring_add 84 84
mirroring_add 85 85
mirroring_add 86 86
mirroring_add 87 87
mirroring_add 88 88
mirroring_add 89 89
mirroring_add 90 90
mirroring_add 91 91
mirroring_add 92 92
mirroring_add 93 93
mirroring_add 94 94
mirroring_add 95 95
mirroring_add 96 96
mirroring_add 97 97
mirroring_add 98 98
mirroring_add 99 99
mirroring_add 100 100
mirroring_add 101 101
mirroring_add 102 102
mirroring_add 103 103
mirroring_add 104 104
mirroring_add 105 105
mirroring_add 106 106
mirroring_add 107 107
mirroring_add 108 108
mirroring_add 109 109
mirroring_add 110 110
mirroring_add 111 111
mirroring_add 112 112
mirroring_add 113 113
mirroring_add 114 114
mirroring_add 115 115
mirroring_add 116 116
mirroring_add 117 117
mirroring_add 118 118
mirroring_add 119 119
mirroring_add 120 120
mirroring_add 121 121
mirroring_add 122 122
mirroring_add 123 123
mirroring_add 124 124
mirroring_add 125 125
mirroring_add 126 126
mirroring_add 127 127
mirroring_add 128 128
mirroring_add 129 129
mirroring_add 130 130
mirroring_add 131 131
mirroring_add 132 132
mirroring_add 133 133
mirroring_add 134 134
mirroring_add 135 135
mirroring_add 136 136
mirroring_add 137 137
mirroring_add 138 138
mirroring_add 139 139
mirroring_add 140 140
mirroring_add 141 141
mirroring_add 142 142
mirroring_add 143 143
mirroring_add 144 144
mirroring_add 145 145
mirroring_add 146 146
mirroring_add 147 147
mirroring_add 148 148
mirroring_add 149 149
mirroring_add 150 150
mirroring_add 151 151
mirroring_add 152 152
mirroring_add 153 153
mirroring_add 154 154
mirroring_add 155 155
mirroring_add 156 156
mirroring_add 157 157
mirroring_add 158 158
mirroring_add 159 159
mirroring_add 160 160
mirroring_add 161 161
mirroring_add 162 162
mirroring_add 163 163
mirroring_add 164 164
mirroring_add 165 165
mirroring_add 166 166
mirroring_add 167 167
mirroring_add 168 168
mirroring_add 169 169
mirroring_add 170 170
mirroring_add 171 171
mirroring_add 172 172
mirroring_add 173 173
mirroring_add 174 174
mirroring_add 175 175
mirroring_add 176 176
mirroring_add 177 177
mirroring_add 178 178
mirroring_add 179 179
mirroring_add 180 180
mirroring_add 181 181
mirroring_add 182 182
mirroring_add 183 183
mirroring_add 184 184
mirroring_add 185 185
mirroring_add 186 186
mirroring_add 187 187
mirroring_add 188 188
mirroring_add 189 189
mirroring_add 190 190
mirroring_add 191 191
mirroring_add 192 192
mirroring_add 193 193
mirroring_add 194 194
mirroring_add 195 195
mirroring_add 196 196
mirroring_add 197 197
mirroring_add 198 198
mirroring_add 199 199
mirroring_add 200 200
mirroring_add 201 201
mirroring_add 202 202
mirroring_add 203 203
mirroring_add 204 204
mirroring_add 205 205
mirroring_add 206 206
mirroring_add 207 207
mirroring_add 208 208
mirroring_add 209 209
mirroring_add 210 210
mirroring_add 211 211
mirroring_add 212 212
mirroring_add 213 213
mirroring_add 214 214
mirroring_add 215 215
mirroring_add 216 216
mirroring_add 217 217
mirroring_add 218 218
mirroring_add 219 219
mirroring_add 220 220
mirroring_add 221 221
mirroring_add 222 222
mirroring_add 223 223
mirroring_add 224 224
mirroring_add 225 225
mirroring_add 226 226
mirroring_add 227 227
mirroring_add 228 228
mirroring_add 229 229
mirroring_add 230 230
mirroring_add 231 231
mirroring_add 232 232
mirroring_add 233 233
mirroring_add 234 234
mirroring_add 235 235
mirroring_add 236 236
mirroring_add 237 237
mirroring_add 238 238
mirroring_add 239 239
mirroring_add 240 240
mirroring_add 241 241
mirroring_add 242 242
mirroring_add 243 243
mirroring_add 244 244
mirroring_add 245 245
mirroring_add 246 246
mirroring_add 247 247
mirroring_add 248 248
mirroring_add 249 249
mirroring_add 250 250
mirroring_add 251 251
mirroring_add 252 252
mirroring_add 253 253
mirroring_add 254 254
mirroring_add 255 255
