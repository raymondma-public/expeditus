//IP and TCP ID
#define ETHERTYPE_IPV4 0x0800
#define IP_PROTOCOLS_TCP 6

//Expeditus Header ID
#define EXPEDITUS_ID 0x9900
#define EXPEDITUS_9901_ID 0x9901



//ECMP datas
#define ECMP_BIT_WIDTH 10
#define ECMP_GROUP_TABLE_SIZE 1024
#define ECMP_SELECT_TABLE_SIZE 16384


//TCP flags code
#define TCP_SYN 0x02
#define TCP_SYN_ACK 0x12
#define TCP_ACK 0x10

//Headers Start=======================
//================
header_type ethernet_t {
    fields {
        dstAddr : 48;
        srcAddr : 48;
        etherType : 16;
    }
}

header ethernet_t ethernet;
//================
header_type ipv4_t {
    fields {
        version : 4;
        ihl : 4;
        diffserv : 8;
        totalLen : 16;
        identification : 16;
        flags : 3;
        fragOffset : 13;
        ttl : 8;
        protocol : 8;
        hdrChecksum : 16;
        srcAddr : 32;
        dstAddr: 32;
    }
}


header ipv4_t ipv4;
//================

header_type tcp_t {
    fields {
        srcPort : 16;
        dstPort : 16;
        seqNo : 32;
        ackNo : 32;
        dataOffset : 4;
        res : 3;
        ecn : 3;
        ctrl : 6;
        window : 16;
        checksum : 16;
        urgentPtr : 16;
    }
}
header tcp_t tcp;
//================
header_type expeditus_t{
    fields{
//        TPID:16;
        nextEtherType:16;
        sf:2;
        noe:6;
        cd:64;
    }
}

header expeditus_t expeditus;


//================
header_type expeditus_9901_t{
    fields{
//        TPID:16;
        nextEtherType:16;
        egressPort:16;
    }
}
header expeditus_9901_t expeditus_9901;

//Headers end=================


//Parsers Definition Start======================
parser start {
    return parse_ethernet;
}


parser parse_expeditus {  
    extract(expeditus);
    return select(latest.nextEtherType) {
        ETHERTYPE_IPV4 : parse_ipv4;
        default: ingress;
    }
}

parser parse_expeditus_9901 {  
    extract(expeditus_9901);
    return select(latest.nextEtherType) {
        ETHERTYPE_IPV4 : parse_ipv4;
        default: ingress;
    }
}



parser parse_ethernet {
    extract(ethernet);
    return select(latest.etherType) {
        ETHERTYPE_IPV4 : parse_ipv4;
        EXPEDITUS_9901_ID : parse_expeditus_9901;
        EXPEDITUS_ID: parse_expeditus;
        default: ingress;
    }
}


parser parse_ipv4 {
    extract(ipv4);
    return select(latest.protocol) {
        IP_PROTOCOLS_TCP : parse_tcp;
        default: ingress;
    }
}



parser parse_tcp {
    extract(tcp);
    return ingress;
}


//End Parsers Definition=============


//Others Start============
field_list ipv4_checksum_list {
        ipv4.version;
        ipv4.ihl;
        ipv4.diffserv;
        ipv4.totalLen;
        ipv4.identification;
        ipv4.flags;
        ipv4.fragOffset;
        ipv4.ttl;
        ipv4.protocol;
        ipv4.srcAddr;
        ipv4.dstAddr;
}

field_list_calculation ipv4_checksum {
    input {
        ipv4_checksum_list;
    }
    algorithm : csum16;
    output_width : 16;
}

calculated_field ipv4.hdrChecksum  {
    verify ipv4_checksum;
    update ipv4_checksum;
}
//Others End============


//Metadata Start=====================


header_type routing_metadata_t {
    fields {
        nhop_ipv4 : 32;
        
        // TODO: if you need extra metadata for ECMP, define it here
    }
}

metadata routing_metadata_t routing_metadata;


header_type user_metadata_t {
    fields {
        switch_role : 4;
        dummy_egress_port:16;
        upstream_num:12;
        tempIp:32;
        tempMac:48;
        tempPort:16;

        tempSrcIpForCloning:32;
        tempSrcPortCloning:48;
        tempDstIpForCloning:32;
        tempDstPortCloning:48;
        tempProtocolCloning:16;

        nextEtherType:16;
    }
}
metadata user_metadata_t user_metadata;

//Metadata End=====================


//Register Start=================
register switch_role{
    width:32;
    static:switch_role_table;
    instance_count:16384 ;
}
//Register End=================


//Actions & Table Start========================

action _drop() {
    drop();
}

action set_nhop(nhop_ipv4, port) {
    modify_field(routing_metadata.nhop_ipv4, nhop_ipv4);
    modify_field(standard_metadata.egress_spec, port);
    add_to_field(ipv4.ttl, -1);
}

action read_switch_role(){
    register_write(switch_role,1,123);
}

action init_switch_role(role_number){
    register_write(switch_role,1,role_number);  
    modify_field(user_metadata.switch_role,role_number);
}

table switch_role_table {

    actions {
        init_switch_role;
        read_switch_role;

    }
    size : 16384;
}


action set_CLI_thrift_port(thrift_port){
    set_thrift_port(thrift_port);
}

table set_CLI_thrift_port_table{
    actions {
        set_CLI_thrift_port;
 
    }
    size : 16384;
}

field_list l3_hash_fields {
    ipv4.srcAddr;
    ipv4.dstAddr;
    ipv4.protocol;
    tcp.srcPort;
    tcp.dstPort;
}

field_list_calculation ecmp_hash {
    input {
        l3_hash_fields;
    }
    algorithm : crc16;
    output_width : ECMP_BIT_WIDTH;
}

//=================================================== ECMP Groups Start(All are the same,because P4 limitation)
table ecmp_group {
    reads {
        ipv4.dstAddr : lpm;
    }
    action_profile: ecmp_action_profile;
    size : ECMP_GROUP_TABLE_SIZE;
}


table expeditus_stage2_aggregate_dst_group {
    reads {
        ipv4.dstAddr : lpm;
    }
    action_profile: ecmp_action_profile;
    size : ECMP_GROUP_TABLE_SIZE;
}


table expeditus_aggregate_group {
    reads {
        ipv4.dstAddr : lpm;
    }
    action_profile: ecmp_action_profile;
    size : ECMP_GROUP_TABLE_SIZE;
}


table expeditus_group {
    reads {
        ipv4.dstAddr : lpm;
    }
    action_profile: ecmp_action_profile;
    size : ECMP_GROUP_TABLE_SIZE;
}

table expeditus_stage1_tor_target_group {
    reads {
        ipv4.dstAddr : lpm;
    }
    action_profile: ecmp_action_profile;
    size : ECMP_GROUP_TABLE_SIZE;
}

table expeditus_tor_remaining_group {
    reads {
        ipv4.dstAddr : lpm;
    }
    action_profile: ecmp_action_profile;
    size : ECMP_GROUP_TABLE_SIZE;
}

//=================================================== ECMP Groups End(All are the same,because P4 limitation)


action_selector ecmp_selector {
    selection_key : ecmp_hash;
}

action_profile ecmp_action_profile {
    actions {
        _drop;
        set_nhop;
    }
    size : ECMP_SELECT_TABLE_SIZE;
    dynamic_action_selection : ecmp_selector;
}

action set_dmac(dmac) {
    modify_field(ethernet.dstAddr, dmac);
}

//==============================================Forward Start(All are the same,because P4 limitation)
table forward {
    reads {
        routing_metadata.nhop_ipv4 : exact;
    }
    actions {
        set_dmac;
        _drop;
    }
    size: 512;
}

table expeditus_stage2_aggregate_dst_forward {
    reads {
        routing_metadata.nhop_ipv4 : exact;
    }
    actions {
        set_dmac;
        _drop;
    }
    size: 512;
}




table expeditus_aggregate_forward {
    reads {
        routing_metadata.nhop_ipv4 : exact;
    }
    actions {
        set_dmac;
        _drop;
    }
    size: 512;
}

table expeditus_stage1_tor_target_forward {
    reads {
        routing_metadata.nhop_ipv4 : exact;
    }
    actions {
        set_dmac;
        _drop;
    }
    size: 512;
}


table expeditus_first_forward {
    reads {
        routing_metadata.nhop_ipv4 : exact;
    }
    actions {
        set_dmac;
        _drop;
    }
    size: 512;
}



table expeditus_tor_remaining_forward {
    reads {
        routing_metadata.nhop_ipv4 : exact;
    }
    actions {
        set_dmac;
        _drop;
    }
    size: 512;
}

//==============================================Forward End(All are the same,because P4 limitation)

field_list clone_fields{
    ethernet;
    ipv4;
    tcp;
    expeditus;
    standard_metadata;
    user_metadata;
}

action compute_and_set_later_forward_egress(num_up_stream){

    //get CD
    //compute egress port for later use, but not directly set egress_spec
    compute_expeditus_egress_spec(user_metadata.dummy_egress_port,expeditus.cd,num_up_stream,ipv4.srcAddr,tcp.srcPort,ipv4.dstAddr,tcp.dstPort,ipv4.protocol);

    //we clone packet here to egress
    clone_ingress_pkt_to_egress(user_metadata.dummy_egress_port,clone_fields);

}

action expeditus_stage2_aggregate_compute_and_set_later_forward_egress(num_up_stream){

    //get CD
    //compute egress port for later use, but not directly set egress_spec
    compute_expeditus_egress_spec(user_metadata.dummy_egress_port,expeditus.cd,num_up_stream,ipv4.srcAddr,tcp.srcPort,ipv4.dstAddr,tcp.dstPort,ipv4.protocol);
    
    modify_field(user_metadata.nextEtherType,expeditus.nextEtherType);
    remove_header(expeditus);
   
    add_header(expeditus_9901);
    modify_field(ethernet.etherType,EXPEDITUS_9901_ID);
    modify_field(expeditus_9901.nextEtherType,user_metadata.nextEtherType);
    modify_field(expeditus_9901.egressPort,user_metadata.dummy_egress_port);


}



table expeditus_stage2_aggregate_src_compute_egress_port{
    actions {
        expeditus_stage2_aggregate_compute_and_set_later_forward_egress;
        _drop;
    }
    size: 512;
}

table expeditus_stage1_tor_target_compute_egress_port{
    actions {
        compute_and_set_later_forward_egress;
        _drop;
    }
    size: 512;
}

action rewrite_mac(smac) {
    modify_field(ethernet.srcAddr, smac);
}

table send_frame {
    reads {
        standard_metadata.egress_port: exact;
    }
    actions {
        rewrite_mac;
        _drop;
    }
    size: 256;
}

table expeditus_send_frame {
    reads {
        standard_metadata.egress_port: exact;
    }
    actions {
        rewrite_mac;
        _drop;
    }
    size: 256;
}
counter my_indirect_increment_counter{
    type: packets;
    static: increment_table;
    instance_count: 16384;
}

counter my_indirect_increment_SYN_counter{
    type: packets;
    static: increment_SYN_table;
    instance_count: 16384;
}


action increment_action() {
    count(my_indirect_increment_counter , standard_metadata.egress_port);
    //drop();
}


action increment_SYN_action() {
    count(my_indirect_increment_SYN_counter , 0);
    //drop();
}

table increment_SYN_table{

    actions{
        increment_SYN_action;
    }
    size:16384;
}

action add_expeditus_tags(){
    add_header(expeditus);
    modify_field(expeditus.nextEtherType, ethernet.etherType);
    modify_field(expeditus.sf, 0);
    modify_field(expeditus.noe, 0);
    my_modify_field(expeditus.cd,0);//get congestion data from C++, second param useless
    modify_field(ethernet.etherType, EXPEDITUS_ID);
}


table expeditus_header_table{
    actions{
        add_expeditus_tags;
    }
}


table increment_table{
    actions{
        increment_action;
    }
    size:16384;
}



action expeditus_forwarding(port){
    modify_field(standard_metadata.egress_spec, port);
}

table forwarding_table{
    reads{
        ipv4.srcAddr:exact;
        tcp.srcPort:exact;
        ipv4.dstAddr:exact;
        tcp.dstPort:exact;
        ipv4.protocol:exact;
        
        
    }
    actions{
        expeditus_forwarding;
    }

    size:16384;
}

action remove_expeditus_header(){
    modify_field(ethernet.etherType, expeditus.nextEtherType);
    remove_header(expeditus);


}

table expeditus_stage1_tor_target_remove_header{
    actions{
        remove_expeditus_header;
    }
    size:16384;
}

action get_upstream_num(upstream_num){
    modify_field(user_metadata.upstream_num,upstream_num);
}

table ingress_upstream_table{
    actions{
        get_upstream_num;
    }
    size:2048;
}

table egress_upstream_table{
    actions{
        get_upstream_num;
    }
    size:2048;
}


action modify_cloned_packet(){


    modify_field(user_metadata.tempPort,tcp.dstPort);
    modify_field(tcp.dstPort,tcp.srcPort);
    modify_field(tcp.srcPort,user_metadata.tempPort);


    modify_field(user_metadata.tempIp,ipv4.dstAddr);
    modify_field(ipv4.dstAddr,ipv4.srcAddr);
    modify_field(ipv4.srcAddr,user_metadata.tempIp);

    modify_field(expeditus.sf,1); 

    modify_field(tcp.ctrl,0);
}


table ToR_modify_cloned_packet{
    actions{
        modify_cloned_packet;
    }
    size:2048;
}

action ToR_src_2nd_stage_action(){
    torAddEntry(standard_metadata.ingress_port,ipv4.srcAddr,tcp.srcPort,ipv4.dstAddr,tcp.dstPort,ipv4.protocol);
    _drop();
}

table ToR_src_2nd_stage_table{
    actions{
        ToR_src_2nd_stage_action;
    }
    size:2048;
}

action Aggregate_dst_3rd_stage_action(){
    torAddEntry(standard_metadata.ingress_port,ipv4.srcAddr,tcp.srcPort,ipv4.dstAddr,tcp.dstPort,ipv4.protocol);
    _drop();
}

table Aggregate_dst_3rd_stage_table{
    actions{
        Aggregate_dst_3rd_stage_action;
    }
    size:2048;
}

action noOp(){
    
}
table SYN_ACK_table{
    actions{
        noOp;
    }
    size:2048;
}

table ACK_table{
    actions{
        noOp;
    }
    size:2048;
}



action expeditus_stage2_aggregate_dst_header_action(){
    my_modify_field(expeditus.cd,0);
}

table expeditus_stage2_aggregate_dst_header_table{
    actions{
        expeditus_stage2_aggregate_dst_header_action;
    }
    size:2048;
}


action Aggregate_clone_egress_to_egress_action(){
    clone_egress_pkt_to_egress(expeditus_9901.egressPort,clone_fields);
}

table Aggregate_clone_egress_to_egress_table{
    actions{
        Aggregate_clone_egress_to_egress_action;
    }
    size:2048;
}

action Aggregate_stage_3_modify_fields_action(){
    //store current data
    modify_field(user_metadata.tempSrcIpForCloning,ipv4.srcAddr);
    modify_field(user_metadata.tempDstIpForCloning,ipv4.dstAddr);

    modify_field(user_metadata.tempSrcPortCloning,tcp.srcPort);
    modify_field(user_metadata.tempDstPortCloning,tcp.dstPort);

    modify_field(ipv4.srcAddr,user_metadata.tempDstIpForCloning);
    modify_field(ipv4.dstAddr,user_metadata.tempSrcIpForCloning);

    modify_field(tcp.srcPort,user_metadata.tempDstPortCloning);
    modify_field(tcp.dstPort,user_metadata.tempSrcPortCloning);

}

table Aggregate_stage_3_modify_fields_table{
     actions{
        Aggregate_stage_3_modify_fields_action;
    }
    size:2048;
}

table show_9901_table{
    actions{
        noOp;
    }
    size:2048;
}




control ingress {
    //get upstream number from CLI
    apply(ingress_upstream_table);

    //Logs=================
    if(tcp.ctrl==TCP_SYN){ 
        apply(increment_SYN_table);
    }

    if(tcp.ctrl==TCP_SYN_ACK){
        apply(SYN_ACK_table);
    }

    if(tcp.ctrl==TCP_ACK){
        apply(ACK_table);
    }

    if(ethernet.etherType==EXPEDITUS_9901_ID){
        apply(show_9901_table);
    }
    //Logs End==============



    //forwarding table
    apply(forwarding_table){

        hit{

        }


        miss{
            apply(switch_role_table);//get switch role
            //Only ToR and aggregate switch needs 
            if(user_metadata.switch_role==1 or user_metadata.switch_role==2){//tor or aggregate
                apply(set_CLI_thrift_port_table);
            }

            //Due to P4 limitation: cannot use table more than once
            //Use complex condition for apply table for multiple cases
            if(user_metadata.switch_role==0 or user_metadata.switch_role==3 // ecmp or core


            or //tor target remaining cases
             ((user_metadata.switch_role==1) and
                    (tcp.ctrl!=TCP_SYN) and
                    ethernet.etherType!=EXPEDITUS_ID )

                and not (ethernet.etherType==EXPEDITUS_9901_ID)

            ){ //ECMP or Core
                if(valid(ipv4) and ipv4.ttl > 0) {

                    apply(ecmp_group);
                    apply(forward);

                   
                }
            }else if(user_metadata.switch_role==1){//Expeditus-ToR
                
                //the counter show P4 recgonized SYN
                if(tcp.ctrl==TCP_SYN){ 
                    if(ethernet.etherType==EXPEDITUS_ID){   //Has expeditus header
                                                            //Target ToR 1st stage
                        apply(expeditus_stage1_tor_target_compute_egress_port);  
                        apply(expeditus_stage1_tor_target_group);
                        apply(expeditus_stage1_tor_target_forward);

                    }else{  //Do not contain expeditus header
                            //Source ToR 1st stage

                        apply(expeditus_group);
                        apply(expeditus_first_forward);

                    }
                    
                }

                else if(ethernet.etherType==EXPEDITUS_9901_ID){//can also check from upstream
                    apply(ToR_src_2nd_stage_table);
                }


            }else if(user_metadata.switch_role==2){//Expeditus-Aggregate

                if(ethernet.etherType==EXPEDITUS_ID){//
                    if(expeditus.sf==1){//stage 2
                         //may also need to check
                         if(standard_metadata.ingress_port<=user_metadata.upstream_num and standard_metadata.ingress_port>0){     //if ingress port <= upstream, then from (upstream
                              apply(expeditus_stage2_aggregate_src_compute_egress_port);
                     
                         }else if(standard_metadata.ingress_port>user_metadata.upstream_num){    //from down stream

                                apply(expeditus_stage2_aggregate_dst_header_table);
                                apply(expeditus_stage2_aggregate_dst_group);
                                apply(expeditus_stage2_aggregate_dst_forward);
                         }

                    }else if(expeditus.sf==2 and standard_metadata.ingress_port<=user_metadata.upstream_num and standard_metadata.ingress_port>0){

                    }

                }

                if(ethernet.etherType==EXPEDITUS_9901_ID and standard_metadata.ingress_port<=user_metadata.upstream_num and standard_metadata.ingress_port>0){
                    apply(Aggregate_dst_3rd_stage_table);
                }

                //when it is aggregate, all stage need group and forward
                //but some stage need to do extra computation or modify field 
                //to make forwarding table
                apply(expeditus_aggregate_group);
                apply(expeditus_aggregate_forward);
            }//end aggregate

        }

    }

        

}


control egress {

    apply(egress_upstream_table);

    apply(increment_table);

    if(user_metadata.switch_role==0 or user_metadata.switch_role==3){//ECMP or Core

    }else if(user_metadata.switch_role==1){//Expeditus-ToR
         if(tcp.ctrl==TCP_SYN){ 


                    if (((ethernet.etherType==EXPEDITUS_ID)) and (standard_metadata.egress_port<=user_metadata.upstream_num) and (standard_metadata.egress_port>0))  {//this is cloned packet cloned from this ToR
                            apply(ToR_modify_cloned_packet);
                    }else

                    if(ethernet.etherType==EXPEDITUS_ID){   //Has expeditus header
                                                            //Target ToR 1st stage

                            apply(expeditus_stage1_tor_target_remove_header);                                                            
                    }
                    
                    else{  //Do not contain expeditus header
                            //Source ToR 1st stage
                        if((standard_metadata.egress_port<=user_metadata.upstream_num) and (standard_metadata.egress_port>0)){
                            //if upstream add header
                            apply(expeditus_header_table);
                        }else{
                            //if down strea do nothing
                        }
                        
                    }
                                            

                    }

    }else if(user_metadata.switch_role==2){//Expeditus-Aggregate
        if(ethernet.etherType==EXPEDITUS_9901_ID and standard_metadata.egress_port<=user_metadata.upstream_num and standard_metadata.egress_port>0){
            //goto upstream
            apply(Aggregate_stage_3_modify_fields_table);
        }else if(ethernet.etherType==EXPEDITUS_9901_ID and standard_metadata.egress_port>=user_metadata.upstream_num){
            //goto downstream
            apply(Aggregate_clone_egress_to_egress_table);
        }

    }


}


