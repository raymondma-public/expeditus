table_set_default send_frame _drop
table_set_default forward _drop
table_add send_frame rewrite_mac 1 => 00:0c:00:00:00:12
table_add send_frame rewrite_mac 2 => 00:0c:00:00:00:13
table_add send_frame rewrite_mac 253 => 00:0c:00:00:00:15
table_add send_frame rewrite_mac 254 => 00:0c:00:00:00:14
table_add send_frame rewrite_mac 133 => 00:0d:00:00:00:02
table_add forward set_dmac 10.1.0.22 => 00:0b:00:00:00:0e
table_add forward set_dmac 10.1.0.25 => 00:0b:00:00:00:11
table_add forward set_dmac 10.0.4.4 => 00:04:00:00:04:00
table_add forward set_dmac 10.0.5.4 => 00:04:00:00:05:00
table_add forward set_dmac 10.1.133.1 => 00:0d:00:00:00:01
table_indirect_create_group ecmp_group
table_indirect_create_group ecmp_group
table_indirect_create_group ecmp_group
table_indirect_create_group ecmp_group
table_indirect_create_member ecmp_group _drop
table_indirect_create_member ecmp_group set_nhop 10.1.0.22 1
table_indirect_create_member ecmp_group set_nhop 10.1.0.25 2
table_indirect_create_member ecmp_group set_nhop 10.0.4.4 254
table_indirect_create_member ecmp_group set_nhop 10.0.5.4 253
table_indirect_create_member ecmp_group set_nhop 10.1.133.1 133
table_indirect_add_member_to_group ecmp_group 1 0
table_indirect_add_member_to_group ecmp_group 2 0
table_indirect_add_member_to_group ecmp_group 3 1
table_indirect_add_member_to_group ecmp_group 4 2
table_indirect_add_member_to_group ecmp_group 5 3
table_indirect_set_default ecmp_group 0
table_indirect_add_with_group ecmp_group 10.0.1.4/32 => 0
table_indirect_add_with_group ecmp_group 10.0.2.4/32 => 0
table_indirect_add_with_group ecmp_group 10.0.4.4/32 => 1
table_indirect_add_with_group ecmp_group 10.0.5.4/32 => 2
