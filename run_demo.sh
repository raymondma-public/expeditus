#!/bin/bash

# Copyright 2013-present Barefoot Networks, Inc. 
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

source $THIS_DIR/../../env.sh

#P4C_BM_SCRIPT=$P4C_BM_PATH/p4c_bm/__main__.py

SWITCH_PATH=/home/raymondma/behavioral-model/targets/simple_switch/simple_switch

CLI_PATH=/home/raymondma/behavioral-model/tools/runtime_CLI.py

# Probably not very elegant but it works nice here: we enable interactive mode
# to be able to use fg. We start the switch in the background, sleep for 2
# minutes to give it time to start, then add the entries and put the switch
# process back in the foreground
set -m
sudo p4c-bmv2 p4src/simple_router.p4 --json simple_router.json
if [ $? -ne 0 ]; then
echo "p4 compilation failed"
exit 1
fi
# This gets root permissions, and gives libtool the opportunity to "warm-up"
sudo $SWITCH_PATH >/dev/null 2>&1

#The -i 1@veth2 is the
#-i : interface
#1 : port 1
#veth2 : veth2.pcap
#monitor port 1 of switch, and store all packet of port 1 as veth2.pcap
#if change to other vethX, not work
#if no -i, not work
sudo $SWITCH_PATH simple_router.json \
    -i 0@veth0 -i 1@veth2 -i 2@veth4 -i 3@veth6 -i 4@veth8 \
   --log-console \
    --pcap &
# -i 0@veth0 -i 1@veth1 -i 2@veth2 -i 3@veth3 -i 4@veth4 \


sleep 2
echo "**************************************"
echo "Sending commands to switch through CLI"
echo "**************************************"
# $CLI_PATH --json simple_router.json < commands.txt
echo "READY!!!"
fg
